/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author estudiantes
 */
public class Profesor extends Persona {
    private String materia;

    @Override
    public String toString() {
        return "Profesor{" + "materia=" + materia + '}';
    }

    public Profesor(String materia, int dni, String nombre) {
        super(dni, nombre);
        this.materia = materia;
    }

    public Profesor() {
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }
    
}
