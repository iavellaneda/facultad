/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author estudiantes
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona p = new Persona(2233234,"Juan Gomez");
        System.out.println(p.getNombre());
        Profesor profe = new Profesor("Fisica", 2234134, "Jose De La Cruz");
        System.out.println("Profesor: " + profe.getNombre());
        Alumno alu = new Alumno("Ing En Sist", 23123443, "Juan Perez");
        System.out.println(alu.getNombre());
        System.out.println(alu.getCarrera());
        // Se puede hacer esto ya que profe esta contenido en persona
        Persona unaPersona = profe;
        // El metodo que se va a ejecutar es el de profe
        System.out.println(unaPersona.toString());
        
    }
    
}
