/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author estudiantes
 */
public class Alumno extends Persona {

    public Alumno(String carrera, int dni, String nombre) {
        super(dni, nombre);
        this.carrera = carrera;
    }

    @Override
    public String toString() {
        return "Alumno{" + "carrera=" + carrera + '}';
    }

    public Alumno() {
    }
    
    private String carrera;           

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
}
