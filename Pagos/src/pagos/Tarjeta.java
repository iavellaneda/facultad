/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

/**
 *
 * @author estudiantes
 */
public class Tarjeta {
    private String tipo;
    private int numero;
    private int mesVenc;

    @Override
    public String toString() {
        return "Tarjeta{" + "tipo=" + tipo + ", numero=" + numero + ", mesVenc=" + mesVenc + ", a\u00f1oVenc=" + añoVenc + '}';
    }

    
    public Tarjeta(String tipo, int numero, int mesVenc, int añoVenc) {
        this.tipo = tipo;
        this.numero = numero;
        this.mesVenc = mesVenc;
        this.añoVenc = añoVenc;
    }

    public Tarjeta() {
    }
    private int añoVenc;
    
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getMesVenc() {
        return mesVenc;
    }

    public void setMesVenc(int mesVenc) {
        this.mesVenc = mesVenc;
    }

    public int getAñoVenc() {
        return añoVenc;
    }

    public void setAñoVenc(int añoVenc) {
        this.añoVenc = añoVenc;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
