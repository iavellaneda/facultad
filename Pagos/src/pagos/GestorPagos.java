/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

import java.util.ArrayList;

/**
 *
 * @author estudiantes
 */
public class GestorPagos {
    private ArrayList<Pago> listaPagos; 

    public GestorPagos() {
       listaPagos = new ArrayList<Pago>();
    }

    public GestorPagos(ArrayList<Pago> listaPagos) {
        this.listaPagos = listaPagos;
    }

    public ArrayList<Pago> getListaPagos() {
        return listaPagos;
    }

    public void setListaPagos(ArrayList<Pago> listaPagos) {
        this.listaPagos = listaPagos;
    }
    
    public void agregarPago(Pago p) {
        this.listaPagos.add(p);
    }
    
    public float sumarPagos() {
        float total = 0;
        for(Pago item: listaPagos){
            total += item.getMonto();
        }
        return total;
    }
    
}
