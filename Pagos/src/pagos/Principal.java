/*
 * Paradigmas de Programacion.
 * Ejemplo de pagos, usando herencia, constructores, arraylists, etc.
 */
package pagos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Ignacio Avellaneda
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cliente c1 = new Cliente("Juan Perez", 11234212); 
        Tarjeta t1 = new Tarjeta("VISA", 223123211, 11, 2019);
        PagoTarjeta pt = new PagoTarjeta(c1, 1500, "TV 40 pulgadas", new Date(2018, 8, 6) , t1, 6);
        PagoCheque pc = new PagoCheque(c1, 6530, "Licuadora", new Date(2018, 8, 9), 0, "Macro", 90);
        System.out.println(pt);                     
        GestorPagos gp = new GestorPagos();
        gp.agregarPago(pc);
        gp.agregarPago(pt);
        System.out.println(gp.sumarPagos());
    }    
}
