/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

import java.util.Date;

/**
 *
 * @author estudiantes
 */
public class Pago {
    private Cliente cliente;
    private String concepto;

    public Pago(Cliente cliente, String concepto, float monto, Date fecha) {
        this.cliente = cliente;
        this.concepto = concepto;
        this.monto = monto;
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Pago>> " + cliente + ", concepto: " + concepto + ", monto: $" + monto + ", fecha: " + fecha;
    }

    public Pago() {
    }
    private float monto;
    private Date fecha;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
