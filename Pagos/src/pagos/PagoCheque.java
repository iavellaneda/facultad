/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

import java.util.Date;

/**
 *
 * @author estudiantes
 */
public class PagoCheque extends Pago {

    public PagoCheque(Cliente cl, float monto, String concepto, Date fecha, int numero, String banco, int dias) {
        super(cl, concepto, monto, fecha);
        this.numero = numero;
        this.banco = banco;
        this.dias = dias;
    }

    public PagoCheque() {
    }

    @Override
    public String toString() {
        return "Cheque{" + "numero=" + numero + ", banco=" + banco + ", dias=" + dias + '}';
    }
    private int numero;
    private String banco;
    private int dias;    

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
}
