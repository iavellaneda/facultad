/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

import java.util.Date;

/**
 *
 * @author estudiantes
 */
public class PagoTarjeta extends Pago {

    public PagoTarjeta() {
    }

    public PagoTarjeta(Cliente cliente, float monto, String concepto, Date Fecha, Tarjeta t, int cuotas) {
        super(cliente, concepto, monto, Fecha);        
        this.tarjeta = t;
        this.cuotas = cuotas;
    }
    private Tarjeta tarjeta;
    private int cuotas;

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }

    
}
